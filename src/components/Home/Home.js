import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Home = () => {
    return (
        <div>
            <h1 className="animate__animated animate__heartBeat">Welcome to the Trivia Challenge!</h1>
            <br /><br /><br /><br /><br /><br />
            <h5>You will be presented with 10 <span className="c-g">True</span> or <span className="c-r">False</span> questions.</h5>
            <br /><br /><br /><br /><br /><br />
            <h3 className="animate__animated animate__pulse animate__infinite">Can you score 100%?</h3>
            <br /><br /><br /><br /><br /><br />
            <Button variant="primary" className="animate__animated animate__fadeInUp">
                <Link to="/quiz" style={{'color': 'white', 'textDecoration': 'none'}}><h4>BEGIN</h4></Link>
            </Button>
        </div>
    )
}

export default Home;