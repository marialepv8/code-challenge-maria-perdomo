import './Result.css';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import he from 'he';

const Results = ( props ) => {
  const { quizValues, score } = props;
      
  return (
    <div>
      <h2>You scored <br /> <span className={score > 5 ? "c-g" : "c-r"}>{score}</span> / {quizValues.length} </h2>
      <br /><br />
      <div>
        {
          quizValues.map((quiz, idx) => (
            <div key={idx} className="result">
              <Container className="container-width">
                <Row className="justify-content-md-center">
                  <Col xs={1}>
                    {
                      quiz.correct_answer === quiz.value ?
                      (
                        <div className="icon c-g">
                          +
                        </div>
                      ) :
                      (
                        <div className="icon c-r">
                          -
                        </div>
                      )
                    }
                  </Col>
                  <Col className={quiz.correct_answer === quiz.value ? "c-g" : "c-r"}>
                    <p className="t-j">{he.decode(quiz.question)}</p>
                  </Col>
                </Row>
              </Container>
            </div>
          ))
        }
      </div>
      <br /><br />
      <Link to="/">
        <h2 className="animate__animated animate__pulse animate__infinite">PLAY AGAIN?</h2>
      </Link>
    </div>
  )
}

export default Results;