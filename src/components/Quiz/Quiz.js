import './Quiz.css';
import React, { useState, useEffect } from "react";
import { Button, Container, Row, Col } from 'react-bootstrap';
import Results from './Results';
import he from 'he';

const Quiz = () => {
  const [quizzes, setQuizzes] = useState([]);
  const [initialIndex, setInitialIndex] = useState(0);
  const [quizValues] = useState([]);
  const [score, setScore] = useState(0);

  useEffect(() => {
    const getQuiz = async () => {
      const res = await fetch(`${process.env.REACT_APP_BACKEND_API}`);
      const data = await res.json();
      setQuizzes(data.results);
    }

    getQuiz();
  }, [])

  const setValues = (index, value, quizValue) => {
    setInitialIndex(index+1);
    quizValues.push({index, value, question: quizValue.question, correct_answer: quizValue.correct_answer});
    if(value === quizValue.correct_answer) {
      setScore(score+1);
    }
  }

  return (
    <div>
      {
        quizzes.map((quiz, index) => (
          <div key={index}>
            {
              index === initialIndex && (
                <div>
                  <h2>{quiz.category}</h2>
                  <br /><br /><br /><br /><br /><br />
                  <Container className="container-width">
                    <Row className="justify-content-md-center">
                      <Col className="question">
                        <p className="animate__animated animate__backInRight">{he.decode(quiz.question)}</p>
                        <br /> <br /><br /><br />
                        <Row>
                          <Col><Button variant="danger" onClick={() => setValues(index, 'False', quiz)} 
                          className="btn-false animate__animated animate__headShake animate__infinite">False</Button></Col>
                          <Col><Button variant="success" onClick={() => setValues(index, 'True', quiz)} 
                          className="btn-true animate__animated animate__headShake animate__infinite">True</Button></Col>
                        </Row>
                      </Col>
                    </Row>
                  </Container>
                  <br />
                  <h6>{index+1} of {quizzes.length}</h6>
                </div>
              )
            }
          </div>
        ))
      }
      {
        initialIndex+1 > quizzes.length && (
          <Results quizValues={quizValues} score={score} />
        )
      }
    </div>
  )
}

export default Quiz;