import './App.css';
import { Container } from 'react-bootstrap';
import { Route, Switch } from 'react-router-dom';
import Home from '../Home/Home';
import Quiz from '../Quiz/Quiz';

const App = () => {
  return (
    <Container fluid>
      <Switch>
        <Route exact path="/" component={Home}>
          <Home />
        </Route>
        <Route path="/quiz" component={Quiz}>
          <Quiz />
        </Route>
      </Switch>
    </Container>
  );
}

export default App;
