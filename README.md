First thing to use in the project is run 'npm install'

## Package installed:

npm install dotenv-webpack --save
This one is to use the enviroment, to change the port or add constants to use in the project, for example:
PORT = '2000'
REACT_APP_BACKEND = 'ip'

npm install react-bootstrap@next bootstrap@5.1.0

npm install react-router-dom

npm install he --save
to convert the entities that not got the chance to change with UTF-8
for more information https://github.com/mathiasbynens/he

## To use transitions

animate.css
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />

To use it in the page only have to add "animate__animated animate__(animation)" 
example:
className="animate__animated animate__pulse"
if want to dalay the animation "animate__delay-1s"
if want to continue the animation "animate__infinite"